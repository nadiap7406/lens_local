from starlette.applications import Starlette
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import JSONResponse


import spacy
import api
import numpy as np

nlp = spacy.load('en_core_web_lg', disable = ['ner', 'tagger',  "lemmatizer"])


import uvicorn
import os
import gc
import json
import random
import re
import io
import os
import base64
from io import BytesIO
from PIL import Image
# Imports the Google Cloud client library
# Imports the Google Cloud client library
from google.cloud import vision

# Instantiates a client
client = vision.ImageAnnotatorClient()

middleware = [
    Middleware(CORSMiddleware,    
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"])
]

app = Starlette(debug=True, middleware=middleware)

book_api = api.SemanticSimilarityAPI(nlp, 300)
harry_potter = book_api.load("harry_potter", nlp)

# Needed to avoid cross-domain issues
response_header = {
    'Access-Control-Allow-Origin': '*'
}



generate_count = 0

def getLabels(content):
    image = vision.Image(content=content)
    metadata = {}
    objectlabels = []
        # Performs label detection on the image file
    response = client.label_detection(image)
    labels = response.label_annotations
    print('Labels:')
    textlabels = []
    for label in labels:
        print(label.description)
        textlabels.append(label.description)

    

    if response.error.message:
        raise Exception(
            '{}\nFor more info on error messages, check: '
            'https://cloud.google.com/apis/design/errors'.format(
                response.error.message))


    objects = client.object_localization(
        image=image).localized_object_annotations

    print('Number of objects found: {}'.format(len(objects)))
    for object_ in objects:
        obj = {}
        obj["name"] = object_.name
        obj["score"] = object_.score
        print('\n{} (confidence: {})'.format(object_.name, object_.score))
        print('Normalized bounding polygon vertices: ')
        obj["vertices"] = []
        for vertex in object_.bounding_poly.normalized_vertices:
            obj["vertices"].append({'x': str(vertex.x), 'y': str(vertex.y)})
        objectlabels.append(obj)

    metadata["labels"] = textlabels
    metadata["objects"] = objectlabels


    # Get any text in image
    ocrresponse = client.text_detection(image=image)
    texts = ocrresponse.text_annotations
    print('Texts:')

    ocr = []

    for text in texts:
        print('\n"{}"'.format(text.description))

        ocr.append(text.description)

        vertices = (['({},{})'.format(vertex.x, vertex.y)
                    for vertex in text.bounding_poly.vertices])

        print('bounds: {}'.format(','.join(vertices)))
    if ocrresponse.error.message:
        raise Exception(
            '{}\nFor more info on error messages, check: '
            'https://cloud.google.com/apis/design/errors'.format(
                response.error.message))
    metadata["ocr"] = ocr
    return metadata


@app.route('/', methods=['GET', 'POST', 'HEAD'])
async def homepage(request):


    # global generate_count
    # global sess

    params = await request.json()
    imageBase64 = params.get('img')
    image_data = re.sub('^data:image/.+;base64,', '',imageBase64)
    base64Data = base64.b64decode(image_data)
    # im = Image.open()
    labels = getLabels(base64Data)

    if len(labels)==0:
        return JSONResponse({"error": "no labels returned"},
                                    headers=response_header)

    # Get random three labels
    randomlabels = random.sample(labels["labels"], 3)

    quotes = harry_potter.response_for(randomlabels[0]+" "+randomlabels[1]+" " + randomlabels[2], 10)
    labels["quote"] = quotes
    output = json.dumps(labels)
    # print(imageBase64)

    return JSONResponse({'labels': output},
                            headers=response_header)



if __name__ == '__main__':
    uvicorn.run(app, host='0.0.0.0', port=int(os.environ.get('PORT', 8080)))

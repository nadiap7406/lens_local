import spacy
from simpleneighbors import SimpleNeighbors
import numpy as np
import random

#@title Text Source
#@markdown Forms support many types of fields.

text_source = 'harry_potter.txt'  #@param {type: "string"}
print(text_source.rsplit('.', 1)[0])

# Class to manage
class SemanticSimilarity:
    
    def __init__(self, nlp, dims):
        self.nns = SimpleNeighbors(dims)
        self.id_pairs = {}
        self.vocab = []
        self.dims = dims
        self.nlp = nlp
        
    def add_to_vocab(self, item):
        cur_id = len(self.vocab)
        self.vocab.append(item)
        return cur_id
    
    def add_pair(self, first, second):
        first_id = self.add_to_vocab(first)
        second_id = self.add_to_vocab(second)
        self.id_pairs[first_id] = second_id
        vec = self.vectorize(first)
        self.nns.add_one(first_id, vec)
    
    def add_sentence(self, sentence ):
      id = self.add_to_vocab(sentence)    
      vec = self.vectorize(sentence)
      self.nns.add_one(id, vec)   
    
    def vectorize(self, s):
        if s == "":
            s = " "
        doc = self.nlp(s, disable=['tagger', 'parser'])
        mean = np.mean(np.array([w.vector for w in doc]), axis=0)
        return mean
    
    def build(self, n=50):
        self.nns.build(n)
        
    def response_for(self, s, n=10):
        vec = self.vectorize(s)
        nearest_ids = self.nns.nearest(vec, n)
        picked = random.choice(nearest_ids)
        return self.vocab[picked]
    
    def save(self, prefix):
        import pickle
        data = {
            'vocab': self.vocab,
            'dims': self.dims
        }
        with open(prefix + "-chatbot.pkl", "wb") as fh:
            pickle.dump(data, fh)
        self.nns.save(prefix)
        
    @classmethod
    def load(cls, prefix, nlp):
        import pickle
        with open(prefix + "-chatbot.pkl", "rb") as fh:
            data = pickle.load(fh)
            newobj = cls(nlp, data['dims'])
            # newobj.id_pairs = data['id_pairs']
            newobj.vocab = data['vocab']
            newobj.nns = SimpleNeighbors.load(prefix)
        return newobj


nlp = spacy.load('en_core_web_lg', disable = ['ner', 'tagger',  "lemmatizer"])


# Read text
text_url = "./" + text_source
text = open(text_url, "r").read()


# Split into sentences
words = []
text = text[0:500000]
doc = nlp(text)
i=0
doc_sents = [sent for sent in doc.sents]
print(doc_sents[100])

# Pick a random amount of them
print(len(doc_sents))
size = 15000 if len(doc_sents)>15000 else len(doc_sents)
random_sentences = random.sample(doc_sents, size)


# Train the semantic model

# if api:
  # del api


api = SemanticSimilarity(nlp, 300)

for i, sentence in enumerate(random_sentences):
    api.add_sentence(str(sentence.text))
api.build()

# api.response_for("hahaha fucker")
api.save(text_source.rsplit('.', 1)[0])


# del api

api2 = SemanticSimilarity(nlp, 300)
thing = api2.load("harry_potter", nlp)
print(thing.response_for("I'm going to go get some coffee."))